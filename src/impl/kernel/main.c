#include "print.h"
#include "keyboard.h"
#include "string.h"
#include "system.h"

void ENTRY() {
    print_str("$ ");
}

void help_command() {
    print_str("\nAvailable commands:\n");
    print_str("HELP   - Show this help message\n");
    print_str("CLEAR  - Clear the screen\n");
    print_str("ABOUT  - Show system information\n");
}

void about_command() {
    print_str("\nBSK Kernel v0.1\n");
    print_str("64-bit x86 Kernel\n");
    print_str("Author: Avishek\n");
}

void sysinfo_command() {
    char buffer[20];
    print_str("\nSystem Information:\n");
    print_str("CPU: ");
    print_str((char*)get_cpu_brand());
    print_str("\nMemory: ");
    print_str(itoa(get_total_memory(), buffer, 10));
    print_str(" KB\n");
}

void kernel_main(multiboot_info_t* mb_info) {
    print_clear();
    print_set_color(PRINT_COLOR_YELLOW, PRINT_COLOR_BLACK);
    print_str("Welcome to our 64-bit kernel. Developed by Avishek!\n");
    print_set_color(PRINT_COLOR_LIGHT_RED, PRINT_COLOR_BLACK);
    print_str("Type HELP to see all available commands\n\n");
    print_set_color(PRINT_COLOR_WHITE, PRINT_COLOR_BLACK);
    
    init_system_info(mb_info);
    
    while(1) {
        ENTRY();
        char* input = get_input();
        
        if (strcmp(input, "HELP") == 0) {
            help_command();
        } else if (strcmp(input, "CLEAR") == 0) {
            print_clear();
        } else if (strcmp(input, "ABOUT") == 0) {
            about_command();
        } else if (strcmp(input, "SYSINFO") == 0) {
            sysinfo_command();
        } else if (input[0] != '\0') {
            print_str("\nUnknown command: ");
            print_str(input);
        }
        print_str("\n");
    }
}
