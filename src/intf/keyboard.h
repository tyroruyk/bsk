#pragma once

#include <stdint.h>

void keyboard_handler();
char* get_input();
uint8_t inb(uint16_t port); 