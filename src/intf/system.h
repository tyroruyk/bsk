#pragma once
#include <stdint.h>

typedef struct {
    uint32_t size;
    uint32_t reserved;
    uint64_t addr;
    uint64_t len;
    uint32_t type;
} __attribute__((packed)) multiboot_memory_map_t;

typedef struct {
    uint32_t flags;
    uint32_t mmap_length;
    uint32_t mmap_addr;
    // ... other multiboot2 fields ...
} multiboot_info_t;

void init_system_info(multiboot_info_t* mb_info);
const char* get_cpu_brand();
uint32_t get_total_memory(); 