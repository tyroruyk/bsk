# Basic System Kernel - BSK

A Basic Operating System Kernel

# Compilation

Compile BSK With GCC in any GNU/Linux Distribution

# Dependencies

> gcc
> ld
> nasm
> xorriso
> grub

# Compilation

`make build-x86_64`
